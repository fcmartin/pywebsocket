import json
import geventwebsocket

from flask_jwt_extended.utils import verify_token_claims
from flask_jwt_extended.view_decorators import _decode_jwt_from_headers


class SocketClient:
    """single connected client.
    contains the websocket and a blocking method that listens
    for incomming messages.

    keyword agruments:
    message_handles -- function to handle incomming messages

    public methods:
    start_listening_forever -- blocking method that keeps listening for 
        incomming messages
    """

    def __init__(self, ws: geventwebsocket.websocket.WebSocket, db_id: int, 
                 message_handler=None):
        self.id = db_id
        self.ws = ws

        # default message handling
        self.message_handler = message_handler if message_handler else self.__log
        
    def start_listening_forever(self):
        while not self.ws.closed:

            # recieve message from the connected client, blocking
            message = self.ws.receive()

            # TODO we should verify the header Bearer token here
            # if the message is None, the client terminated the connection
            if message:
                message = json.loads(message)
                self.message_handler(message)
    
    def __log(self, msg: str):
        print(f"[SocketClient]> {msg}")


class SocketClientsList:
    """Websocket client manager.
    Keeps track of all connected clients and provides an interface to send
    messages to all or individual clients.
    
    Keyword arguments:
    message_handler -- function that handles incomming messages

    Public properties:
    clients -- list of connected SocketClient objects

    Public methods:
    broadcast(msg) -- broadcast a message to all connected clients
    send_message_to(id, msg) -- send message to specified client
    append(client) -- add new SocketClient to the manager
    remove(client) -- remove SocketClient from the manager
    client_from_incomming_connection(ws) -- create new client from incomming websocket connection
    """

    # contains all connected clients
    clients = []

    def __init__(self, message_handler=None):
        self.__message_handler = message_handler

    def broadcast(self, message: dict):
        self.__log("> broadcasting to all clients")
        # print("> " + message)
        for client in self.clients:
            client.ws.send(json.dumps(message))
    
    def __get_client(self, idd: int):
        for client in self.clients:
            if client.id == idd:
                return client
        return None

    def send_message_to(self, idd: int, message: dict):
        client = self.__get_client(idd)
        client.ws.send(json.dumps(message))
        
    def append(self, client: SocketClient):
        self.clients.append(client)

    def remove(self, client: SocketClient):
        self.clients.remove(client)

    def client_from_incomming_connection(self, ws: geventwebsocket.websocket.WebSocket):

        # check header from message for api-key,
        # this should be a refreshable key from the RESTful API
        # preferable we should not have to send a message first....
        message = ws.receive()
        self.__log(f"> authmessage = {message}")

        # Retrieve header and get claims
        jwt_data = _decode_jwt_from_headers()

        # verify that the user claims are valid
        verify_token_claims(jwt_data)

        # hook 'em up
        client_id = jwt_data['identity']['id']
        client = SocketClient(ws, client_id, message_handler=self.__message_handler)
        self.append(client)

        # return the client
        return client

    def __log(self, msg: str):
        print(f"[SocketClientsList]: {msg}")


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# DEMONSTRATION CODE
if __name__ == "__main__":
    
    import datetime
    
    from flask import Flask, jsonify, request
    from geventwebsocket.handler import WebSocketHandler
    from gevent import pywsgi
    from flask_sockets import Sockets
    from flask_jwt_extended import (JWTManager, create_access_token,
                                    create_refresh_token, get_jwt_identity,
                                    jwt_refresh_token_required)

    # init flask, jwt and sockets
    app = Flask(__name__)
    app.config['JWT_SECRET_KEY'] = 'f8a87430-fe18-11e7-a7b2-a45e60d00d91'
    jwt = JWTManager(app)
    sockets = Sockets(app)

    # Dummy Database
    nodes = [
        {"id": 1, "name": "node1", "api_key": "somekey1"},
        {"id": 2, "name": "node2", "api_key": "somekey2"},
        {"id": 3, "name": "node3", "api_key": "somekey3"},
    ]

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # WEBSOCKET endpoints
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    # method to handle incomming messages
    def message_handler(message: dict):
        if message['msg_type'] == "print":
            print(message['msg'])

    # keeps track of all connected clients
    clients = SocketClientsList(message_handler=message_handler)

    # endpoint to which clients can connect
    @sockets.route('/echo')
    def echo_socket(ws: geventwebsocket.websocket.WebSocket):
        """"this is called every time a new clients connects"""

        # add new client, and identificate this client
        print("> new client connected, trying to authenticate")
        global clients
        client = clients.client_from_incomming_connection(ws)

        # blocking until connection is closed
        client.start_listening_forever()

        # client disconnected
        clients.remove(client)
        print("> socket closed the connection")


    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # FLASK endpoints
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


    # Endpoint to retrieve a fresh Bearer token. Expirery time is 
    # set really short to demonstrate the refreshing of the client
    # token
    @app.route("/token", methods=['POST'])
    def token():
        if request.is_json:
            api_key = request.json.get('api_key', None)
            # get node
            node = None
            for nod in nodes:
                if nod['api_key'] == api_key:
                    node = nod
                    break

            # generate JWT tokens
            if node:
                return jsonify({
                    'access_token': create_access_token(node, expires_delta=datetime.timedelta(seconds=10)),
                    'refresh_token': create_refresh_token(node)
                }), 200  # ok
            else:
                return {"msg": "node not found..."}, 400
        else:
            return {"msg": "api-key not found"}, 400

    # Endpoint to refresh a Bearer token.
    @app.route("/refresh", methods=['POST'])
    @jwt_refresh_token_required
    def refresh():
        """Create a token from a refresh token."""
        identity = get_jwt_identity()
        if identity:
            access_token = create_access_token(
                identity,
                expires_delta=datetime.timedelta(seconds=15)
            )
            print(f"refresh token={access_token}")
            return jsonify({
                'access_token': access_token,
                'refresh_token': create_refresh_token(identity)
            })

        return {"msg": "user not found...!"}, 400

    # Endpoint to return a list of connected client. It also notifies 
    # all connected clients with a broadcasting message and sends a 
    # message specifically to client 1.
    @app.route('/')
    def hello():
        html = ""
        global clients
        for client in clients.clients:
            html += "client id = " + str(client.id) + " is connected! <br> "

        # send message to all clients
        clients.broadcast(
            {"msg_type": "print", "msg": "your information wat displayed on the webpage"})

        # send message to client id = 1
        clients.send_message_to(
            1, {"msg_type": "print", "msg": "this is a message specifically for node id=1"})

        return html

    # Start the WSGI server
    server = pywsgi.WSGIServer(('', 5000), app, handler_class=WebSocketHandler)
    server.serve_forever()
