import websocket
import json
import time
import queue

from threading import Thread


class SocketService:
    """Initializes and maintains a websocket connection with the server.
    After connecting (in a seperate thread), the service keeps listening 
    to the server. In case it is disconnected, the service tries to reconnect (token refresh).
    The service contains a message queue, which sends the messages when 
    connected. Upon a reconnect all messages will be send at once. In case an
    expired token is used a new token should be requested through the token_callback
    function

    public properties:
    token -- the bearer authorization token, update to use a new token
    token_callback -- function to generate new JWT bearer token

    public methods:
    add_message(msg_type, msg) -- adds a new <msg> from <msg_type> to the queue
    """

    def __init__(self, url="ws://localhost:5000/echo", token=None, token_callback=None):
        """Initializes the websocket connection with the server.
        It creates two seperate threads, one to listen for incomming messages
        and another for sending messages.

        keyword arguments:
        url -- the url of the websocket-server
        token -- JWT bearer token needed for authentication at the server
        """

        # store initial token        
        self.token = token

        # setup the websocket
        self.__connected = False
        self.__websocket = self.__setup_websocket(url)

        # connect websocket in a thread
        self.receive_message_que = queue.Queue()
        self.__start_websocket_worker()

        # Threaded service that sends messages from the que
        self.send_message_que = queue.Queue()
        self.__start_broadcasting_worker()

        # Attach token callback function
        self.token_callback = token_callback


    def add_message(self, message_type, message):
        """Appends a message to the message que."""
        self.send_message_que.put({
            'msg_type': message_type,
            'msg': message
        })

    def __setup_websocket(self, url):
        # configure websocket
        ws = websocket.WebSocketApp(
            url,
            header={"Authorization": f"Bearer {self.token}"},
            on_message=self.__on_message,
            on_error=self.__on_error,
            on_close=self.__on_close
        )
        # websocket.enableTrace(True)
        ws.on_open = self.__on_open
        return ws

    def __start_websocket_worker(self):
        # start websocket connection in seprate thread
        t = Thread(target = self.__connect_to_server)
        t.daemon = True # terminate thread when app ends
        t.start()
        
    def __connect_to_server(self):
        # attempt to maintain connection
        while True:
            self.__websocket.run_forever()
            
            # server disconnected, we need to update
            # the header as the token might be expired
            self.__log("disconnected from server")
            self.__log("trying to reconnect in 10 seconds")
            time.sleep(10)
            if self.token_callback:
                try:
                    self.__log("attempting to update the token")
                    self.__update_token()
                except:
                    self.__log("refreshing token failed")
                
    
    def __update_token(self):
        # try to obtain a new token through the call-back
        self.token = self.token_callback()
        # update websocket header with the new token
        self.__websocket.header = {"Authorization": f"Bearer {self.token}"}

    def __on_message(self, ws, message):
        # message is none when disconnected
        if message:
            message = json.loads(message)
            self.receive_message_que.put(message)

    def __on_open(self, ws):
        # send message to let the server know wo we are
        # this is done by the header, therefore an empty message is enough
        self.__websocket.send("Here you have my authorization token...")
        self.__connected = True

    def __on_error(self, ws, error):
        self.__log(f"error = {error}")

    def __on_close(self, ws):
        self.__connected = False
        self.__log("Connection closed")

    def __start_broadcasting_worker(self):
        # start sending message service in seperate thread
        t = Thread(target=self.__send_message)
        t.daemon = True
        t.start()

    def __send_message(self):
        while True:
            if self.__connected:

                # blocking
                message = self.send_message_que.get()

                # send the message
                message = json.dumps(message)
                self.__websocket.send(message)

    def __log(self, message):
        print(f"[client]> {message}")


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# DEMONSTRATION CODE 
if __name__ == "__main__":
    
    import requests

    # Dummy database in order to simulate multiple clients
    nodes = [
        {"api_key": "somekey1"},
        {"api_key": "somekey2"},
        {"api_key": "somekey3"}
    ]

    # 
    # WEBSOCKET CUSTOM CALLBACK FUNCTIONS
    # 

    # function to handle incomming messages from the server. They 
    # always should come in the form of an dict that contains a
    # msg_type and msg. 
    def actions(message):
        if message["msg_type"] == "print":
            print(f"from the handler={message['msg']}")
        elif message["msg_type"] == "call":
            if message["msg"] == "some_trigger_function":
                some_trigger_function()
            elif message["msg"] == "some_other_trigger_function":
                some_other_trigger_function()
            else:
                print("function to call is not available through sockets")
        else:
            print(f"msg_type={message['msg_type']} from server not recognized. How did that happen!?")


    # call-back to get a new Bearer token in order to connect 
    # to the server through websockets
    def refresh_token_callback():
        
        response = requests.post('http://localhost:5000/refresh', headers={
            "Authorization": f"Bearer {refresh_token}"
        })
        response_data = response.json()
        return response_data['access_token']

    # functions to demonstate how to trigger a function from
    # a message recieved through a websocket
    def some_trigger_function():
        print("called some trigger function")

    def some_other_trigger_function():
        print("called other trigger function")
    

    #
    # CREATE/CONNECT CLIENTS AND DO SOME MESSAGING
    #
    
    # function to authentice on the servers RESTful API. 
    def authenticate(api_key="somekey1"):
        # make POST request with autorization key to obtain Bearer token
        response = requests.post('http://localhost:5000/token', json={
            'api_key': api_key
        })
        
        if response.status_code == 200:
            response_data = response.json()
            access_token = response_data['access_token']
            refresh_token = response_data['refresh_token']

            return access_token, refresh_token
        
        # most likely the api_key was invalid or server was down
        return None
    
    # Connect all dummy nodes by (1) obtain an bearer token through the 
    # RESTful API (2) connect the socket using this authorization token 
    connected = []
    for node in nodes:
        token, refresh_token = authenticate(node['api_key'])
        connected.append(
            SocketService('ws://localhost:5000/echo', token, 
                          token_callback=refresh_token_callback)
        )
    
    # Send a message to the first connected client. Usually you connect
    # only once to a servers. You might connect to different servers.
    for i in range(5):
        connected[0].add_message('print', f'hoi!! {i}')
        
    # keep checking for new actions in the que
    while True:
        
        time.sleep(1)

        # because we have multiple clients connected we need to 
        # check for each client if they have new messages
        for client in connected:
            # non blocking
            if not client.receive_message_que.empty():
                message = client.receive_message_que.get()
                actions(message)
